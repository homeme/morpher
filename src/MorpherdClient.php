<?php

namespace HomeMe\Morpher;

use Guzzle\Http\Client;
use Guzzle\Http\Exception\RequestException;
use InvalidArgumentException;

/**
 * Client for morpherd
 */
final class MorpherdClient implements Morpher
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @param string $host
     * @param int $port
     */
    public function __construct($host, $port)
    {
        $this->httpClient = new Client("http://{$host}:{$port}");
    }

    /**
     * Склонение слов по падежам
     *
     * @param string $text
     * @param string $case
     * @throws InvalidArgumentException
     * @return string
     */
    public function inflect($text, $case)
    {
        $request = $this->httpClient->get('/morphology/inflect');

        $request->getQuery()->replace(['text' => $text, 'case' => $case]);

        try {
            $response = $request->send();
        } catch (RequestException $e) {
            throw new \RuntimeException('Unexpected HTTP request error: ' . $e->getMessage(), 0 , $e);
        }

        $response = $response->json();

        if (!isset($response['response'], $response['success'])) {
            throw new \RuntimeException('Unexpected answer format: ' . json_encode($response));
        }

        if (!$response['success']) {
            throw new InvalidArgumentException($response['response']['errorMessage']);
        }

        return $response['response']['result'];
    }

    /**
     * Определение пола по имени/слову.
     *
     * @param string $text
     * @throws InvalidArgumentException
     * @return string
     */
    public function gender($text)
    {
        $request = $this->httpClient->get('/morphology/gender');

        $request->getQuery()->replace(['text' => $text]);

        try {
            $response = $request->send();
        } catch (RequestException $e) {
            throw new \RuntimeException('Unexpected HTTP request error: ' . $e->getMessage(), 0 , $e);
        }

        $response = $response->json();

        if (!isset($response['response'], $response['success'])) {
            throw new \RuntimeException('Unexpected answer format: ' . json_encode($response));
        }

        if (!$response['success']) {
            throw new InvalidArgumentException($response['response']['errorMessage']);
        }

        return $response['response']['result'];
    }
}