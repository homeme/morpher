<?php

namespace HomeMe\Morpher;

use InvalidArgumentException;

final class LocalMorpher implements Morpher
{
    /**
     */
    public function __construct()
    {
        if (!function_exists('morpher_inflect') || !function_exists('morpher_get_gender')) {
            throw new \RuntimeException('Internal server error: morpher not found');
        }
    }

    /**
     * Склонение слов по падежам
     *
     * @param string $text
     * @param string $case
     * @throws InvalidArgumentException
     * @return string
     */
    public function inflect($text, $case) {
        $result = morpher_inflect((string)$text, (string)$case);

        if (substr($result, 0, 7) === '#ERROR:') {
            throw new \InvalidArgumentException($result);
        }

        return $result;
    }

    /**
     * Определение пола по имени/слову.
     *
     * @param string $text
     * @throws InvalidArgumentException
     * @return string
     */
    public function gender($text) {
        $result = morpher_get_gender((string)$text);

        if (substr($result, 0, 7) === '#ERROR:') {
            throw new \InvalidArgumentException($result);
        }

        return $result;
    }
}