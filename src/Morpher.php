<?php

namespace HomeMe\Morpher;

use InvalidArgumentException;

interface Morpher
{
    /**
     * родительный падеж
     */
    const CASE_ROD = 'rod';

    /**
     * дательный падеж
     */
    const CASE_DAT = 'dat';

    /**
     * винительный падеж
     */
    const CASE_VIN = 'vin';

    /**
     * творительный падеж
     */
    const CASE_TVOR = 'tvor';

    /**
     * предложный падеж без предлога
     */
    const CASE_PREDL = 'predl';

    /**
     * предложный падеж с предлогом о/об/обо
     */
    const CASE_PREDLO = 'predl-o';

    /**
     * местный падеж (отвечает на вопрос где?)
     */
    const CASE_GDE = 'gde';

    /**
     * Мужской род
     */
    const GENDER_MALE = 'm';

    /**
     * Женский род
     */
    const GENDER_FEMALE = 'f';

    /**
     * Средний род
     */
    const GENDER_NEUTER = 'n';

    /**
     * Множественное число
     */
    const GENDER_PLURAL = 'p';

    /**
     * Склонение слов по падежам
     *
     * @param string $text
     * @param string $case
     * @throws InvalidArgumentException
     * @return string
     */
    public function inflect($text, $case);

    /**
     * Определение пола по имени/слову.
     *
     * @param string $text
     * @throws InvalidArgumentException
     * @return string
     */
    public function gender($text);
}